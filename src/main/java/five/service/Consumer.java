package five.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.StringInputStream;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.command.TransactionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import java.io.*;
import java.util.Optional;

@Component
public class Consumer {
    private static final String ACCEPTED_BUCKET = "task-five-accepted-bucket";
    private static final String REJECTED_BUCKET = "task-five-rejected-bucket";

    private static final Logger log = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    private AmazonS3 amazonS3;

    @JmsListener(destination = "accepted-queue")
    public void receiveAcceptedMessage(ActiveMQTextMessage acceptedMessage) {
        try {
            log.info("Accepted message is: {}", acceptedMessage);
            processMessage(acceptedMessage, ACCEPTED_BUCKET);
        } catch (Exception exception) {
            log.error("Receive accepted message:", exception);
        }
    }

    @JmsListener(destination = "rejected-queue")
    public void receiveRejectedMessage(ActiveMQTextMessage rejectedMessage){
        try {
            log.info("Rejected message is: {}", rejectedMessage);
            processMessage(rejectedMessage, REJECTED_BUCKET);
        } catch (Exception exception) {
            log.error("Receive accepted message:", exception);
        }
    }

    private void processMessage(ActiveMQTextMessage message, String bucket) throws IOException, JMSException {
        if (message == null) {
            throw new IllegalArgumentException("Message is not found.");
        }
        final String key = String.join("_",
                String.valueOf(message.getTimestamp()),
                Optional.ofNullable(message.getTransactionId())
                    .map(TransactionId::getTransactionKey)
                    .orElse("noTransactionId"));
        uploadFile(bucket, key, message.getText());
    }

    private void uploadFile(String bucketName, String keyName, String content) throws IOException {
        if(!amazonS3.doesBucketExist(bucketName)) {
            amazonS3.createBucket(bucketName);
        }

        try (InputStream stream = new StringInputStream(content)) {
            amazonS3.putObject(new PutObjectRequest(bucketName, keyName, stream, null));
        }
    }

}
